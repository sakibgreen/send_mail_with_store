
    <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          @foreach($all_posts as $post)
          <div class="post-preview">
  
            <p> {{ $post->name }} </p>
            <p> {{ $post->company }} </p>
            <p> {{ $post->email }} </p>
            <p> {{ $post->phone }} </p>
            <p> {{ $post->address }} </p>
            <p> {{ $post->website }} </p>
            </h2>

            <!-- <div class="widget-products-item col-xs-12 col-sm-6 col-xl-4">
              <img class="card-img-top" src="{{ asset("storage/upload/".$post->image_name)}}" width="100%" height="350" alt="Card image cap">
            </div> -->

            <div class="col-lg-8 col-md-10 mx-auto">
              @foreach(explode('|', $post->images) as $detail )
                <div class="widget-products-item col-xs-12 col-sm-6 col-xl-4">
                  <a href="{{ asset('storage/upload/' . $detail) }}" class="widget-products-image">
                    <img src="{{ asset('storage/upload/' . $detail) }}" style="max-height: 300px">
                    <span class="widget-products-overlay"></span>
                  </a>
                </div>
              @endforeach
            </div>
          </div>
            <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
          @endforeach
        </div>
      </div>  
      <!--paginate-->
      <div class="text-center">
        {!! $all_posts->links() !!}
      </div>
    </div>
    <hr>
 