<div>
	<form class="form-horizontal mt-5" action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
	{{csrf_field()}}
	<fieldset>
		
		<!-- name input-->
		<div class="form-group">
		  <label class="col-md-12 control-label" for="name">Your Name</label>  
		  <div class="col-md-12">
		  <input id="name" name="name" type="text" placeholder="Write your name" class="form-control input-md" required="">
		  </div>
		</div>

		<!-- company input-->
		<div class="form-group">
		  <label class="col-md-12 control-label" for="company">Company Name</label>  
		  <div class="col-md-12">
		  <input id="company" name="company" type="text" placeholder="Write your company " class="form-control input-md">
		  </div>
		</div> 

		<!-- email input-->
		<div class="form-group">
		  <label class="col-md-12 control-label" for="email">Your email address</label>  
		  <div class="col-md-12">
		  <input id="email" name="email" type="text" placeholder="Write your email" class="form-control input-md" required="">
		  </div>
		</div>

		<!-- phone input-->
		<div class="form-group">
		  <label class="col-md-12 control-label" for="phone">Your phone number</label>  
		  <div class="col-md-12">
		  <input id="phone" name="phone" type="text" placeholder="Write your phone" class="form-control input-md" required="">
		  </div>
		</div>

		<!-- address input-->
		<div class="form-group">
		  <label class="col-md-12 control-label" for="address">Your Address</label>  
		  <div class="col-md-12">
		  <input id="address" name="address" type="text" placeholder="Write your address" class="form-control input-md" required="">
		  </div>
		</div>

		<!-- website input-->
		<div class="form-group">
		  <label class="col-md-12 control-label" for="website">Website</label>  
		  <div class="col-md-12">
		  <input id="website" name="website" type="text" placeholder="Write your website" class="form-control input-md">
		  </div>
		</div>

		<!--File-->

		<!-- <input type="file" name="image_name"> <br> -->

		<div class="form-group">
      <label for="file_images">Files :</label>
      <input type="file" id="images" name="images[]" multiple required />
		</div>
	
		<!-- Send Mail Body-->
		<form action="send" method="post">
			{{csrf_field()}}
			
			<!-- to: <br> <input type="text" name="to"> <br> -->

			Discription : <br> <textarea name="message" cols="30" rows="10"></textarea>
		</form>

		<!-- Button -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for=""></label>
		  <div class="col-md-4">
		    <button class="btn btn-primary btn-sm btn-block">Send</button>
		  </div>
		</div>

	</fieldset>
	</form>
</div>

 

 