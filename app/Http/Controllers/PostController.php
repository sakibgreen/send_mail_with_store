<?php

namespace App\Http\Controllers;

use App\User;
use App\Post; 

use Mail;
use App\Mail\SendMail;

use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
		$all_posts = Post::orderby('id','desc')->paginate(2);
		
		//dd($all_posts);
    	return view('posts.index',compact('all_posts'));
    }

    public function create()
    {
    	return view('posts.create');
    }

    // public function store(Request $request)
    // {
    // 	if ($request->hasFile('image_name')) {
    // 		$image_name = $request->image_name->getClientOriginalName();
    // 		$image_size = $request->image_name->getClientSize()/1024/1024;
    // 		$request->image_name->storeAs('public/upload',$image_name);

    // 		$posts 							= new Post;
	// 		$posts->name 					= $request->name;
    // 		$posts->company 				= $request->company;
    // 		$posts->email 					= $request->email;
    // 		$posts->phone 					= $request->phone;
    // 		$posts->address 				= $request->address; 
    // 		$posts->website 				= $request->website;			
    // 		$posts->image_name				= $image_name;
    // 		$posts->image_size				= $image_size;
	// 		$posts->save();
			
	// 		Mail::send(new SendMail("discription"));

    // 	}
    // 	return redirect()->route('posts.index');
	// }
	

    public function store(Request $request)
    {
        $this->validate($request,
            [
				'name'           => 'required|max:255',
                'email'          => 'required|email',				
                'phone'          => 'required|max:11',
                'address'        => 'max:255',
            ]);

        $images = array();
        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {

                $name = $image->getClientOriginalName();
                $image->storeAs('public/upload', $name);
                $images[] = $name;

            }
        }

        Post::create([
			'name'          => request('name'),
            'company'       => request('company'),
            'email'         => request('email'),
            'phone'         => request('phone'),
            'address'       => request('address'),
            'website'       => request('website'),
            'images'        => implode("|", $images),
            			
		]);
		Mail::send(new SendMail("discription"));

       return redirect()->route('posts.index');
    }

	
}
