<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    public function users()
    {
    	return $this->belongsTo(User::class,'user_id');
    }

    public function user_name($id)
    {
   		$name = User::find($id);
   		if (@$name){
   			return $name->name;
   		}
   		else
   			return "Null";
    }
}